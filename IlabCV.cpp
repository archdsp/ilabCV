#include "IlabCV.hpp"

namespace ILAB
{
    namespace cv
    {
        // constructor 
        Matrix::Matrix()
        {
        }
        
        Matrix::Matrix (unsigned int row,
                        unsigned int col,
                        unsigned int channel,
                        int dataType,
                        unsigned char* data)
        {
            size_t dataSize = row * col * channel;
            void *tmp = NULL;

            if (dataSize <= 0)
                return;

            // allocate data
            this->data = (unsigned char*) malloc(sizeof(unsigned char) * dataSize);
            if (this->data != NULL)
            {
                this->row = row;
                this->col = col;
                this->channel = channel;
                this->dataType = dataType;
                memmove(this->data, data, sizeof(unsigned char) * dataSize);   
            }            
        }


        /* public function */
        size_t Matrix::GetDataSize(void)
        {
            return this->row * this->col * this->channel;
        }

        /* destructor */
        Matrix::~Matrix()
        {
            //free(this->data);
        }
        
    } // end namespace cv
} // end namespace ILAB

