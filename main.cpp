#include <iostream>
#include <cstring>
#include <cstdio>
#include <string>

// for cam use. not really need
#include <opencv2/opencv.hpp>

#include "IlabCV.hpp"

namespace icv = ILAB::cv;

bool openCVMat2kornicCVMartrix(const cv::Mat src, icv::Matrix& dst)
{
    // 복사할 데이터 없는 경우 에러처리
    if (src.data == NULL)
        return false;

    // 복사할 데이터 사이즈가 맞지 않는 경우
    size_t srcDataSize = src.rows * src.cols * src.channels();
    if (srcDataSize <= 0)
        return false;

    // 할당 데이터 없는 경우 메모리 할당
    if (dst.data == NULL)
        dst.data = (unsigned char*) malloc(sizeof(unsigned char) * srcDataSize);

    // 할당이 되어있지만, 사이즈 확장/축소가 필요한 경우 메모리 재할당
    else if (dst.GetDataSize() != srcDataSize)
    {
        unsigned char* tmp = (unsigned char*) realloc (dst.data, sizeof(size_t) * srcDataSize);
        if (tmp == NULL)
            return false;
        dst.data = tmp;
    }

    // 데이터 할당이 온전히 이루어 지면, 값 대입 후 true 리턴
    if (dst.data != NULL)       
    {
        dst.col = src.rows;
        dst.row = src.cols;
        dst.channel = src.channels();
        dst.dataType = src.type();
        memmove(dst.data, src.data, sizeof(unsigned char) * dst.GetDataSize());

        return true;
    }

    return false;
}

cv::Mat kornicCVMartrix2openCVMat(const icv::Matrix src)
{
    cv::Mat ret(src.col, src.row, src.dataType, src.data);
    return ret;
}

int main(int argc, char** argv)
{
    cv::VideoCapture cam;
    cv::Mat frame;
    icv::Matrix mat;

    if(!cam.open(1))
        return 0;

    while(cam.isOpened())
    {
        
        cam >> frame;
        if( frame.empty() )
            break; // end of video stream
        
        openCVMat2kornicCVMartrix(frame, mat);
        frame = kornicCVMartrix2openCVMat(mat);
        
        cv::imshow("ret", frame);

        // ESC to stop.
        if( cv::waitKey(10) == 27 )
            break;
    }
    
    return 0;
}
